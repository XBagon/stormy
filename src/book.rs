use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    fs::File,
    io::{self, Write},
    path::Path,
};
use std::collections::HashSet;

#[derive(Deserialize, Serialize)]
pub struct Book {
    pub title: String,
    pub states: Vec<State>,
    #[serde(skip)]
    pub current_state_id: usize,
}

impl Book {
    pub fn new(name: String) -> Self {
        Book {
            title: name,
            states: Vec::new(),
            current_state_id: 0
        }
    }
}

impl Book {
    fn state_content(&self, state_id: usize) -> &str {
        match &self.states[state_id] {
            State::Normal(s) => &s.content,
            State::Redirect(s) => &s.content,
            State::Mirror(s) => {
                if s.title.is_empty() {
                    self.state_content(s.mirrored)
                } else {
                    &s.content
                }
            }
        }
    }

    pub fn current_content(&self) -> &str {
        self.state_content(self.current_state_id)
    }

    fn state_title(&self, state_id: usize) -> &str {
        match &self.states[state_id] {
            State::Normal(s) => &s.title,
            State::Redirect(s) => &s.title,
            State::Mirror(s) => {
                if s.title.is_empty() {
                    self.state_title(s.mirrored)
                } else {
                    &s.title
                }
            }
        }
    }

    pub fn current_title(&self) -> &str {
        self.state_title(self.current_state_id)
    }

    pub fn print_current(&mut self) {
        println!("[{}] {}\n{}", self.current_state_id, self.current_title(), self.current_content());
        if let State::Redirect(s) = &self.states[self.current_state_id] {
            self.current_state_id = s.redirect_to;
            self.print_current();
        }
    }

    pub fn current_insert_decision(&mut self, decision: String, id: usize) {
        match &mut self.states[self.current_state_id] {
            State::Normal(s) => {
                s.decisions.insert(decision, id);
            },
            State::Redirect(_s) => unreachable!(),
            State::Mirror(s) => {
                s.m_decisions.insert(decision, MirrorDecision::Add(id));
            },
        }
    }

    pub fn current_remove_decision(&mut self, decision: &str) {
        match &mut self.states[self.current_state_id] {
            State::Normal(s) => {
                s.decisions.remove(decision);
            },
            State::Redirect(_s) => unreachable!(),
            State::Mirror(s) => {
                if s.m_decisions.remove(decision).is_none() {
                    s.m_decisions.insert(decision.to_owned(), MirrorDecision::Remove);
                }
            },
        }
    }

    pub fn add_normal_state<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(&mut self, lines: &mut U) -> usize {
        println!("Title:");
        let title = lines.next().unwrap().unwrap();
        println!("Content:");
        let content = lines.next().unwrap().unwrap();
        let id = self.states.len();
        self.states.push(State::Normal(NormalState::new(title, content)));
        eprintln!("Successfully created new state with ID [{}]!", id);
        id
    }

    pub fn add_mirror_state<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(&mut self, lines: &mut U, original: usize) -> usize {
        println!("Title:");
        let title = lines.next().unwrap().unwrap();
        println!("Content:");
        let content = lines.next().unwrap().unwrap();
        let id = self.states.len();
        self.states.push(State::Mirror(MirrorState::new(title, content, original)));
        eprintln!("Successfully created new state with ID [{}]!", id);
        id
    }

    pub fn add_redirect_state<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(&mut self, lines: &mut U, redirect_to: usize) -> usize {
        println!("Title:");
        let title = lines.next().unwrap().unwrap();
        println!("Content:");
        let content = lines.next().unwrap().unwrap();
        let id = self.states.len();
        self.states.push(State::Redirect(RedirectState::new(title, content, redirect_to)));
        eprintln!("Successfully created new state with ID [{}]!", id);
        id
    }

    fn state_try_decision(&self, input: &str, state_id: usize) -> Option<usize> {
        match &self.states[state_id] {
            State::Normal(s) => s.decisions.get(input).map(|&id| id),
            State::Redirect(_s) => unreachable!(),
            State::Mirror(s) => {
                if let Some(d) = s.m_decisions.get(input) {
                    match d {
                        MirrorDecision::Add(id) => {
                            Some(*id)
                        },
                        MirrorDecision::Remove => {
                            None
                        }
                    }
                } else {
                    self.state_try_decision(input, s.mirrored)
                }
            },
        }
    }

    pub fn current_try_decision(&self, input: &str) -> Option<usize> {
        self.state_try_decision(input, self.current_state_id)
    }

    fn state_decisions<'a>(&'a self, state_id: usize) -> Box<dyn Iterator<Item=&String> + 'a> {
        match &self.states[state_id] {
            State::Normal(s) => Box::new(s.decisions.keys().map(|k| k)),
            State::Redirect(_s) => unreachable!(),
            State::Mirror(s) => {
                let mut decisions = HashSet::new();
                let mut removed_decisions = HashSet::new();
                for kv in s.m_decisions.iter() {
                    match kv.1 {
                        MirrorDecision::Add(_) => {
                            decisions.insert(kv.0);
                        },
                        MirrorDecision::Remove => {
                            removed_decisions.insert(kv.0);
                        }
                    }
                }
                for s in self.state_decisions(s.mirrored) {
                    decisions.insert(s);
                }

                let decisions : Vec<&String> = decisions.difference(&mut removed_decisions).map(|&s| s).collect();
                Box::new(decisions.into_iter())
            },
        }
    }

    pub fn current_decisions<'a>(&'a self) -> Box<dyn Iterator<Item=&String> + 'a> {
        self.state_decisions(self.current_state_id)
    }

    pub fn save<P: AsRef<Path>>(&self, file: P) -> io::Result<()> {
        File::create(file).unwrap().write_all(&serde_json::to_vec_pretty(self).unwrap()[..])
    }
}

#[derive(Deserialize, Serialize)]
pub enum State {
    Normal(NormalState),
    Redirect(RedirectState),
    Mirror(MirrorState),
}

#[derive(Deserialize, Serialize)]
pub struct NormalState {
    pub title: String,
    pub content: String,
    pub decisions: HashMap<String, usize>,
}

impl NormalState {
    pub fn new(title: String, content: String) -> Self {
        NormalState { title, content, decisions: HashMap::new() }
    }
}

#[derive(Deserialize, Serialize)]
pub struct RedirectState {
    pub title: String,
    pub content: String,
    pub redirect_to: usize,
}

impl RedirectState {
    pub fn new(title: String, content: String, redirect_to: usize) -> Self {
        RedirectState { title, content, redirect_to }
    }
}

#[derive(Deserialize, Serialize)]
pub struct MirrorState {
    pub title: String,
    pub content: String,
    pub mirrored: usize,
    pub m_decisions: HashMap<String, MirrorDecision>,
}

impl MirrorState {
    pub fn new(title: String, content: String, mirrored: usize) -> Self {
        MirrorState { title, content, mirrored, m_decisions: HashMap::new() }
    }
}

#[derive(Deserialize, Serialize)]
pub enum StateType {
    Normal,
    Redirect(usize),
    Mirror(usize, HashMap<String, MirrorDecision>),
}

#[derive(Deserialize, Serialize)]
pub enum MirrorDecision {
    Add(usize),
    Remove
}