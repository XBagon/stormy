mod book;

use crate::book::*;
use std::{
    error, fmt,
    fs::File,
    io::{stdin, stdout, BufRead, Read, Write},
    path::PathBuf,
};
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt(parse(from_os_str))]
    book_path: PathBuf,
    #[structopt(short = "m", long = "migrate")]
    migrate: bool,
}

#[derive(Debug)]
struct Error {
    message: String,
}

impl Error {
    fn new(message: &str) -> Self {
        Error {
            message: message.to_owned(),
        }
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let opt = Opt::from_args();

    if opt.migrate {
        if opt.book_path.exists() {
            let mut buf = String::new();
            {
                let mut book_file = File::open(&opt.book_path).map_err(|err| Box::new(err))?;
                book_file.read_to_string(&mut buf).map_err(|err| Box::new(err))?;
            }
            let mut book : serde_json::Value = serde_json::from_str(&buf).unwrap();
            if let Some(states) = book.get_mut("states") {
                if let Some(states) = states.as_array_mut() {
                    for state in states {
                        if let Some(_) = state.get("title") {
                            let mut map = serde_json::Map::new();
                            map.insert(String::from("Normal"), state.clone());
                            *state = serde_json::Value::Object(map);
                        } else {
                            return Err(Box::new(Error::new("Malformed book! Maybe it is already migrated?")));
                        }
                    }
                    File::create(&opt.book_path).unwrap().write_all(&serde_json::to_vec_pretty(&book).unwrap()[..]).map_err(|err| Box::new(err))?;
                    eprintln!("Successfully migrated book!");
                    return Ok(());
                }
            }
            return Err(Box::new(Error::new("Malformed book!")));
        } else {
            eprintln!("Book doesn't exist yet.");
            eprintln!("Nothing to migrate.");
        }
    }

    let mut stdout = stdout();
    let stdin = stdin();
    let mut lines = stdin.lock().lines();

    let mut book: Book;

    if opt.book_path.exists() {
        let mut book_file = File::open(&opt.book_path).map_err(|err| Box::new(err))?;
        let mut buf = String::new();
        book_file.read_to_string(&mut buf).map_err(|err| Box::new(err))?;
        book = serde_json::from_str(&buf).unwrap();

        println!("Input starting state ID:");
        let starting_state = lines.next().unwrap().unwrap();
        if let Ok(id) = starting_state.parse::<usize>() {
            if book.states.len() > id {
                book.current_state_id = id;
            } else {
                return Err(Box::new(Error::new("A state with this ID doesn't exist yet in this book.")));
            }
        } else {
            return Err(Box::new(Error::new("Failed parsing ID.")));
        }
    } else {
        println!("Book doesn't exist yet.");
        println!("Name of the book:");
        let name = lines.next().unwrap().unwrap();
        book = Book::new(name);
        println!("Create first state");
        book.add_normal_state(&mut lines);
        book.current_state_id = 0;
    }

    println!(r#"Loaded Book: "{}""#, book.title);

    book.print_current();
    print!("> ");
    stdout.flush().unwrap();

    while let Some(line) = lines.next() {
        let line = line.unwrap().to_ascii_lowercase();
        if line.starts_with("/") {
            let mut args = line[1..].split_whitespace();
            match args.next().unwrap() {
                "save" => {
                    book.save(&opt.book_path).map_err(|err| Box::new(err))?;
                    eprintln!("Saved.");
                }
                "quit" => {
                    book.save(&opt.book_path).map_err(|err| Box::new(err))?;
                    return Ok(());
                }
                "goto" => {
                    if let Some(id) = args.next() {
                        if let Ok(id) = id.parse::<usize>() {
                            if id < book.states.len() {
                                book.current_state_id = id;
                            } else {
                                eprintln!("A state with this ID doesn't exist yet in this book.");
                            }
                        } else {
                            eprintln!("Failed parsing ID.");
                        }
                    } else {
                        eprintln!("Missing ID argument.");
                    }
                }
                "decisions" => {
                    let mut decisions : Box<dyn Iterator<Item=&String>> = book.current_decisions();
                    while let Some(phrase) = args.next() {
                        decisions = Box::new(decisions.filter(move|d| d.contains(phrase)));
                    }
                    let output : String = decisions.map(|d| format!("{}, ",d) ).collect();
                    if !output.is_empty() {
                        println!("{}", &output[0..output.len() - 2]);
                    }
                }
                "remove" => {
                    let rest = line[8..].trim();
                    if !rest.is_empty() {
                        book.current_remove_decision(rest);
                        eprintln!("Removed decision.");
                    } else {
                        eprintln!("Missing decision argument.");
                    }
                }
                "newstate" => {
                    book.add_normal_state(&mut lines);
                }
                "help" => {
                    eprintln!("Available commands:\nsave: Saves the book.\nquit: Saves the book and quits.\ngoto <id>: Goes to state with specified ID.\nhelp: Shows available commands.\ndecisions [phrase]: Shows possible decisions that match the search phrase.\nremove <decision>: Removes a decision from current state.\nnewstate: Creates a new state without any decisions connecting to it.");
                }
                other => {
                    eprintln!(r#"Unknown Command: "{}""#, other)
                },
            }
        } else {
            if let Some(id) = book.current_try_decision(&line) {
                book.current_state_id = id;
            } else {
                loop {
                    println!("This decision doesn't exist yet. Add a new decision? ((yes, with [s]tate/[m]irror state/[r]edirect state)/[a]lias/[n]o)");
                    let answer = lines.next().unwrap().unwrap().to_ascii_lowercase();
                    match &answer[..] {
                        "s" => {
                            let id = book.add_normal_state(&mut lines);
                            book.current_insert_decision(line, id);
                            break;
                        }
                        "m" => {
                            println!("Mirror state with ID:");
                            let mirror_id = input_id(&mut lines);
                            let id = book.add_mirror_state(&mut lines, mirror_id);
                            book.current_insert_decision(line, id);
                            break;
                        }
                        "r" => {
                            println!("Redirect to state with ID:");
                            let redirect_id = input_id(&mut lines);
                            let id = book.add_redirect_state(&mut lines, redirect_id);
                            book.current_insert_decision(line, id);
                            break;
                        }
                        "a" => {
                            println!("Alias for state with ID:");
                            let id = input_id(&mut lines);
                            book.current_insert_decision(line, id);
                            eprintln!("Succesfully created new alias!");
                            break;
                        }
                        "n" => {
                            break;
                        }
                        _ => {
                            eprintln!("Invalid.");
                        }
                    }
                }
            }
        }

        book.print_current();
        print!("> ");
        stdout.flush().unwrap();
    }

    Ok(())
}

fn input_id<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: &mut U) -> usize {
    loop {
        let id = lines.next().unwrap().unwrap();
        if let Ok(id) = id.parse::<usize>() {
            break id;
        } else {
            eprintln!("Failed parsing ID.");
        }
    }
}