Simple Text Adventure engine where you play and create the story at the same time.

#### Get Started:

##### Load:
```sh
stormy path-to-book-file
```

##### Create new:
```sh
stormy path-to-book-file -n BookName
```

Then follow instructions and use `/help` for more information, when in decision mode.
The `>` signals that you're in decision mode.

To save changes in the book, you have to use `/save` or `/quit`, otherwise the changes will be lost!